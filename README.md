# Soundbox V2

## Initialisation du projet

`git pull https://git.inpt.fr/leflocan/soundbox`

`sudo apt install ffmpeg`

Copiez toutes les musiques dans le dossier soundbox/flaskr/sounds

## Install

Dans le dossier soundbox ;

`python3 -m venv venv`

`source venv/bin/activate`

`pip install -e .`

`pip3 install --trusted-host pypi.python.org -r requirements.txt`

`python setup.py bdist_wheel`

`flask --app flaskr init-db`

`waitress-serve --call 'flaskr:create_app'`

Le serveur sera accessible sur le port 8080

## Améliorations

- Mettre en place sous Docker (débuté)