FROM python:3.10-bullseye AS base

# File Author / Maintainer
LABEL image.author="antoninlefloch@yahoo.fr"

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apt-get update && apt-get install -y \
    libsdl2-dev \
    libsdl2-mixer-dev \
    && rm -rf /var/lib/apt/lists/*

# Set environment variable to use dummy audio driver
ENV SDL_AUDIODRIVER=dummy

COPY requirements.txt requirements.txt

RUN pip3 install --trusted-host pypi.python.org -r requirements.txt

COPY . /usr/src/app

RUN pip install -e .

RUN python setup.py bdist_wheel

RUN flask --app flaskr init-db

RUN groupadd -r tvn7 && useradd -g tvn7 tvn7 && chown -R tvn7:tvn7 /usr/src/app

USER tvn7

EXPOSE 8080

CMD ["gunicorn", "-b", "0.0.0.0:8080","flaskr:create_app()"]

#RUN waitress-serve --call 'flaskr:create_app'