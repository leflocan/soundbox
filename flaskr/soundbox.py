from flask import (
    Blueprint,
    flash,
    g,
    redirect,
    render_template,
    request,
    url_for,
    jsonify,
    current_app,
)
from werkzeug.exceptions import abort
from flaskr.db import get_db
import pygame
import os
from werkzeug.utils import secure_filename
from pytube import YouTube
from pydub import AudioSegment
from flask_socketio import emit
from . import socketio


bp = Blueprint("soundbox", __name__)

# Variable to store the currently playing song
current_song = None
paused_song = None

# Dictionary to store the count of requests for each song
request_count = {}

pygame.mixer.init()


@socketio.on("connect")
def handle_connect():
    emit("current_song", {"file_name": current_song})


@bp.route("/")
def index():
    db = get_db()
    musiques = db.execute(
        "SELECT id, titre, nomdufichier, volume, category, active FROM musique"
    ).fetchall()
    return render_template("soundbox/index.html", musiques=musiques)


# Function to determine the most requested song
def get_most_requested_song():
    global request_count
    if request_count:
        return max(request_count, key=request_count.get)
    else:
        return None


# Function to play the song
def play_song(file_name):
    global current_song, request_count, paused_song
    paused_song = None
    music_folder = "./flaskr/sounds"
    file_path = os.path.join(music_folder, file_name)
    pygame.mixer.music.load(file_path)
    db = get_db()
    song = db.execute(
        "SELECT volume FROM musique WHERE nomdufichier = ?", (file_name,)
    ).fetchone()
    if not song:
        return jsonify({"success": False, "message": "Song not found"}), 404

    volume = song["volume"]
    pygame.mixer.music.set_volume(volume)
    pygame.mixer.music.play()
    current_song = file_name

    socketio.emit("current_song", {"file_name": file_name})
    song_length = pygame.mixer.Sound(file_path).get_length()

    while pygame.mixer.music.get_busy():
        elapsed_time = pygame.mixer.music.get_pos() / 1000  # Temps écoulé en secondes
        progress_percentage = (
            elapsed_time / song_length
        ) * 100  # Progression en pourcentage*
        print(progress_percentage)
        socketio.emit("song_progress", {"progress": progress_percentage})
        pygame.time.Clock().tick(10)
        continue
    # Once the song finishes playing, check for the most requested song
    if paused_song == None:
        socketio.emit("current_song", {"file_name": ""})
        socketio.emit("song_progress", {"progress": 0})
    most_requested_song = get_most_requested_song()
    if most_requested_song:
        request_count = {}
        play_song(most_requested_song)
    else:
        current_song = None


@bp.route("/play-song", methods=["POST"])
def play_song_route():
    global current_song, request_count
    data = request.json
    file_name = data.get("fileName")

    # Si aucune chanson n'est en cours de lecture, la chanson demandée est jouée.
    if current_song is None:
        play_song(file_name)
    else:
        # Incrémenter le nombre de requêtes pour la chanson demandée
        request_count[file_name] = request_count.get(file_name, 0) + 1
        print(request_count)

    return jsonify({"success": True})


# à faire
@bp.route("/play", methods=["POST"])
def play_sonnette():
    global current_song, request_count
    return jsonify({"success": True})


@bp.route("/stop-song", methods=["POST"])
def stop_song_route():
    global current_song, request_count, paused_song

    # Stop the current song playing.
    if pygame.mixer.music.get_busy():
        # Stop la liste d'attente
        request_count = {}
        # sauvegarde l'état pour reprendre si besion
        paused_song = current_song

        # pause la musique pour de bon.
        current_song = None
        pygame.mixer.music.pause()
        print("stop sound")
    else:
        print("No sound playing")

    return jsonify({"success": True})


@bp.route("/resume-song", methods=["POST"])
def resume_song_route():
    global current_song, paused_song
    print("Paused song ", paused_song)

    # Si il y a une musique en pause suite à stop-song
    if paused_song != None:
        # la musique en pause reprends
        print("resume sound", paused_song)
        current_song = paused_song
        pygame.mixer.music.unpause()
        paused_song = None

        # On reprends aussi l'affichage de l'état de progression
        music_folder = "./flaskr/sounds"
        file_path = os.path.join(music_folder, current_song)
        song_length = pygame.mixer.Sound(file_path).get_length()

        while pygame.mixer.music.get_busy():
            elapsed_time = (
                pygame.mixer.music.get_pos() / 1000
            )  # Temps écoulé en secondes
            progress_percentage = (
                elapsed_time / song_length
            ) * 100  # Progression en pourcentage
            socketio.emit("song_progress", {"progress": progress_percentage})
            pygame.time.Clock().tick(10)  # attente 100ms
            continue

        # si pendant aucune musique n'est mise en pause, on fini proprement
        if paused_song == None:
            socketio.emit("current_song", {"file_name": ""})
            socketio.emit("song_progress", {"progress": 0})

        # si il y a une musique en attente, on la joue
        most_requested_song = get_most_requested_song()
        if most_requested_song:
            request_count = {}
            play_song(most_requested_song)
        else:
            current_song = None

    return jsonify({"success": True})


@bp.route("/parametres", methods=("GET", "POST"))
def parametres():

    # on récupère tous les sons
    db = get_db()
    musiques = db.execute(
        "SELECT id, titre, nomdufichier, active, volume, category FROM musique"
    ).fetchall()

    # on créé un dict associé
    musiques_dict = [
        {
            "id": row["id"],
            "titre": row["titre"],
            "nomdufichier": row["nomdufichier"],
            "active": row["active"],
            "volume": row["volume"],
            "category": row["category"],
        }
        for row in musiques
    ]

    return render_template("soundbox/parametres.html", musiques=musiques_dict)


@bp.route("/update-song", methods=["POST"])
def update_song():
    data = request.json
    # print("update song :", data)
    song_id = data.get("songId")
    titre = data.get("titre")
    active = data.get("active")
    volume = data.get("volume")
    category = data.get("category")

    db = get_db()
    db.execute(
        "UPDATE musique SET titre = ?, active = ?, volume = ?, category = ? WHERE id = ?",
        (titre, active, volume, category, song_id),
    )
    db.commit()

    return jsonify({"success": True, "message": "Song updated successfully"})


"""
Add or remove a sound
-------------------------------------------------------------------------------

"""


@bp.route("/addsound", methods=("GET", "POST"))
def addsound():

    return render_template("soundbox/addsound.html")


UPLOAD_FOLDER = "./flaskr/sounds"
ALLOWED_EXTENSIONS = {"mp3", "wav"}  # Add allowed file extensions here


def allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS


@bp.route("/upload-song", methods=["POST"])
def upload_song(**kwargs):
    """
    Upload a song to the server.

    Parameters:
    **kwargs - The request object containing the file to be uploaded.
         - 'file' : The file object containing the song to be uploaded.

    Returns:
    A JSON response indicating the success or failure of the upload operation.
         - {'success': True, 'message': 'File uploaded successfully'} : The file was successfully uploaded.
         - {'success': False, 'message': 'No file part'} : No file part was found in the request.
         - {'success': False, 'message': 'No selected file'} : No file was selected in the request.
         - {'success': False, 'message': 'Invalid file format'} : The uploaded file has an unsupported format.
    """
    current_app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER
    if "file" not in request.files:
        return jsonify({"success": False, "message": "No file part"})

    file = request.files["file"]

    if file.filename == "":
        return jsonify({"success": False, "message": "No selected file"})

    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(current_app.config["UPLOAD_FOLDER"], filename))

        # Update the database with the filename
        add_song_to_db(filename, filename)

        return jsonify({"success": True, "message": "File uploaded successfully"})
    else:
        return jsonify({"success": False, "message": "Invalid file format"})


@bp.route("/delete-song", methods=["POST"])
def delete_song():
    current_app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER
    data = request.json
    song_id = data.get("songId")

    # Query the database to get the filename of the song
    db = get_db()
    song = db.execute(
        "SELECT nomdufichier FROM musique WHERE id = ?", (song_id,)
    ).fetchone()

    if song:
        filename = song["nomdufichier"]

        # Delete the file from the server
        file_path = os.path.join(current_app.config["UPLOAD_FOLDER"], filename)
        if os.path.exists(file_path):
            os.remove(file_path)

        # Update the database to remove the song
        db.execute("DELETE FROM musique WHERE id = ?", (song_id,))
        db.commit()

        return jsonify({"success": True, "message": "Song deleted successfully"})
    else:
        return jsonify({"success": False, "message": "Song not found"}), 404


@bp.route("/addsound/youtube", methods=["POST"])
def addsound_youtube():
    current_app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER
    data = request.get_json()
    youtube_url = data.get("youtubeUrl")
    if not youtube_url:
        return jsonify({"message": "No URL provided"}), 400

    try:
        yt = YouTube(youtube_url)
        audio_stream = yt.streams.filter(only_audio=True).first()
        temp_filename = secure_filename(yt.title + ".mp4")
        temp_file_path = os.path.join(
            current_app.config["UPLOAD_FOLDER"], temp_filename
        )
        audio_stream.download(
            output_path=current_app.config["UPLOAD_FOLDER"], filename=temp_filename
        )

        # Convert to WAV using pydub
        audio = AudioSegment.from_file(temp_file_path)
        final_filename = secure_filename(yt.title + ".wav")
        final_file_path = os.path.join(
            current_app.config["UPLOAD_FOLDER"], final_filename
        )
        audio.export(final_file_path, format="wav")

        # Remove the temporary file
        os.remove(temp_file_path)

        add_song_to_db(
            yt.title, final_filename
        )  # Use the YouTube title as the song title
        return jsonify({"message": "YouTube audio successfully downloaded"})
    except Exception as e:
        return jsonify({"message": str(e)}), 500


def add_song_to_db(title, filename):
    db = get_db()
    db.execute(
        "INSERT INTO musique (titre, nomdufichier, active, volume, category) VALUES (?, ?, ?, ?, ?)",
        (title, filename, True, 1, "general"),
    )
    db.commit()


@bp.route("/toggle-status", methods=["POST"])
def toggle_status():
    # print("toggle status")
    data = request.get_json()
    song_id = data.get("song_id")
    # print(song_id)
    db = get_db()
    song = db.execute("SELECT active FROM musique WHERE id = ?", (song_id,)).fetchone()
    if song:
        new_status = not song["active"]
        print("new status", new_status)
        db.execute("UPDATE musique SET active = ? WHERE id = ?", (new_status, song_id))
        db.commit()
        return jsonify({"success": True, "new_status": new_status})
    return jsonify({"success": False, "message": "Song not found"}), 404
