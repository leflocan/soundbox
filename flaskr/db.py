import sqlite3
import os
import click
from flask import current_app, g


def get_db():
    if "db" not in g:
        g.db = sqlite3.connect(
            current_app.config["DATABASE"], detect_types=sqlite3.PARSE_DECLTYPES
        )
        g.db.row_factory = sqlite3.Row

    return g.db


def close_db(e=None):
    db = g.pop("db", None)

    if db is not None:
        db.close()


def init_db():
    db = get_db()

    with current_app.open_resource("schema.sql") as f:
        db.executescript(f.read().decode("utf8"))

    sound_folder = "./flaskr/sounds"
    for filename in os.listdir(sound_folder):
        if filename.endswith(
            ".mp3"
        ):  # Change the extension if your music files have a different extension
            title = filename.replace(".mp3", "")  # Extract title from filename
            db.execute(
                "INSERT INTO musique (titre, nomdufichier, active, volume, category) VALUES (?, ?, ?, ?, ?)",
                (
                    title,
                    filename,
                    True,
                    1.0,
                    "general",
                ),  # Assuming active is True by default
            )
        if filename.endswith(
            ".wav"
        ):  # Change the extension if your music files have a different extension
            title = filename.replace(".wav", "")  # Extract title from filename
            db.execute(
                "INSERT INTO musique (titre, nomdufichier, active, volume, category) VALUES (?, ?, ?, ?, ?)",
                (
                    title,
                    filename,
                    True,
                    1.0,
                    "general",
                ),  # Assuming active is True by default
            )
    db.commit()


@click.command("init-db")
def init_db_command():
    """Clear the existing data and create new tables."""
    init_db()
    click.echo("Initialized the database.")


def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)
